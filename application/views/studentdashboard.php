
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<title>Dashboard</title>
	</head>
	<body>
		
		<link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-radius: 16px;">
								<div class="well profile col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
										<figure>
											<img src="" alt="" class="img-circle" style="width:75px;" id="user-img">
										</figure>
										<h5 style="text-align:center;"><strong id="user-name"><?php echo $data['name'];?></strong></h5>
										<p style="text-align:center;font-size: smaller;" id="user-frid"><?php echo $data['id'];?></p>
										<p style="text-align:center;font-size: smaller;overflow-wrap: break-word;" id="user-email"><?php echo $data['email']?></p>
										<p style="text-align:center;font-size: smaller;"><strong>A/C status: </strong><span class="tags" id="user-status">Active</span></p>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
										<p style="text-align:center;font-size: smaller;"><strong>Role</strong></p>
										<p style="text-align:center;font-size: smaller;" id="user-role">
											<?php 
												if($data['role']==='t'){
													echo "Teacher";
												}else{
													echo "Student";
												}
											?>
										</p>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 divider text-center"></div>
										</div>
										<?php echo form_open('studentdashboard'); ?>
										<form method = "POST">
												<button type="submit" name="button" class="btn btn-success btn-block" value= "LOGOUT">Logout</button>
											<!-- <button class="btn btn-success btn-block"><span class="fa fa-plus-circle">Logout</span></button> -->
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>    
				</div>
			</div>
		</div>
	</body>
</html>
