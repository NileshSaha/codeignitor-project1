<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Additional Info</title>
</head>
<body>
	<?php echo validation_errors(); ?>

	<?php echo form_open('/registration/additionalinfo'); ?>
	<form action = '' method = "POST">
		Choose Subject:
		<select name="choose_subject" id="choose_subject">
		<?php
			foreach($subjects as $v):
		?>
			<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
		<?php endforeach; ?>
		</select>
		<br>
		<br>
		Experience:
		<input type="number" name='experience' min='0' max='5'>
		<br>
		<br>
		<input type="submit" value="Submit">
	</form>
</body>
</html>
