<?php
	class Teachersdashboard extends CI_Model{

		public function user_array($id){
			$this->load->database();
			$query = $this->db->get_where("users",array('id'=>$id));
			if($query->num_rows()== 1){
				return ($query->row_array());
			}
		}

		public function logout(){
			$this->load->library('session');
			$this->session->sess_destroy();	
		}

	}
?>
