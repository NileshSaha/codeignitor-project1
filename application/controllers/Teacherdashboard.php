<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	class Teacherdashboard extends CI_Controller{
		public function __construct(){
			parent::__construct();
			//$this->load->library('session');
			$userdata = $this->session->all_userdata();
			if($userdata['role']!='t'){
				redirect('studentdashboard');
			}
		}
		public function index(){
			//$this->load->library('session');
			$this->load->helper('form');
			$userdata = $this->session->all_userdata();
			if($userdata['logged_in']){
				echo "Teacher's Dashboard";
				$this->load->model("teachersdashboard");
				$data['data'] = $this->teachersdashboard->user_array($userdata['id']);
				$this->load->view("teacherdashboard",$data);
				$buttonpressed = $this->input->post('button');
				if($buttonpressed){
					redirect('/logout','auto');
				}	
			}
		}
	}
?>
