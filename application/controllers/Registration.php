<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	class Registration extends CI_Controller{

		public function __construct(){
			parent::__construct();
			//$this->load->library('session');
			// $this->load->database();
			$userdata = $this->session->all_userdata();
			if(isset($userdata['logged_in'])){
				if($userdata['role']== 't'){
					redirect('teacherdashboard');
				}else if($userdata['role']== 's'){
					redirect('studentdashboard');
				}
			}
		}

		public function index(){
			//$this->output->enable_profiler(TRUE);

			$this->load->library('form_validation');
			$this->load->helper('form');
			
			$this->form_validation->set_rules("firstname","First Name","required|alpha");
			$this->form_validation->set_rules("lastname","Last Name","required|alpha");
			$this->form_validation->set_rules("email","Email ID","required|valid_email|is_unique[users.email]");
			$this->form_validation->set_rules("role","Role","required");
			$this->form_validation->set_rules("address","Address","required|max_length[80]|min_length[1]");
			$this->form_validation->set_rules("password","Password","required|alpha_numeric|min_length[3]|max_length[8]");
			$this->form_validation->set_rules("passconf","Confirm Password","required|alpha_numeric|min_length[3]|max_length[8]|matches[password]");
			
			if ($this->form_validation->run() == FALSE){
				$this->load->view('registration');
			}
			else{	
				echo 'success';
				$this->load->model('registrationsuccess');
				$data = $this->registrationsuccess->addData();				
				if($data['role']=='t'){
					$query = $this->db->get('subjects');
					foreach ($query->result() as $row){
						$subjects[]= $row;
					}
					$data['subjects']=$subjects;
					$this->load->view("addtionalinfo", $data);
					//$this->additionalinfo($data['id']);
					$session_registration_data=array(
						'id'=>$data['id'],
					);
					$this->session->set_userdata($session_registration_data);
				}
			}
		}

		public function additionalinfo(){
			$this->load->library('form_validation');
			$this->load->helper('form');
			$id = $this->session->userdata('id');
			
			$this->form_validation->set_rules("choose_subject","Subject","required");
			$this->form_validation->set_rules("experience","Experience","required");

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('addtionalinfo');
			}
			else
			{
				$data = array(
					'userid' => $id,
					'subjectid' => $this->input->post('choose_subject'),
					'experience' => $this->input->post('experience'),
				);
				$this->db->insert('teachers', $data);
				echo "Thank you for Registration!";

			}
		}
	}
?>
