<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	class Login extends CI_Controller{

		public function __construct(){
			parent::__construct();
			//$this->load->library('session');
			$userdata = $this->session->all_userdata();
			if(isset($userdata['logged_in'])){
				if($userdata['role']== 't'){
					redirect('teacherdashboard');
				}else if($userdata['role']== 's'){
					redirect('studentdashboard');
				}
			}
		}

		public function index(){

			$this->load->library('form_validation');
			$this->load->helper('form');

			$this->form_validation->set_rules("email","Email ID","required|valid_email");
			$this->form_validation->set_rules("password","Password","required|alpha_numeric|min_length[3]|max_length[8]");

			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('login');
			}
			else
			{
				$this->load->model('loginsuccess');
				$data = $this->loginsuccess->checkprofession();
				//$this->load->library('session');
				
				$role = $data['role'];
				$id = $data['id'];

				$session_data = array(
					'id'  => $id,
					'role'=>$role,
					'logged_in' => TRUE
				);

				// $this->load->helper('url');
				$this->session->set_userdata($session_data);
				if($role === 't'){ 
					redirect('/teacherdashboard','auto');
					//$this->load->controller('teacherdashboard');
				}else{
					redirect('/studentdashboard','auto'); 
					//$this->load->controller('studentdashboard');
				}
			}
		}
	}
?>
