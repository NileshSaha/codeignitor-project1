<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	class Studentdashboard extends CI_Controller{
		public function __construct(){
			parent::__construct();
			//$this->load->library('');
			$userdata = $this->session->all_userdata();
			if($userdata['role']!='s'){
				redirect('teacherdashboard');
			}
		}
		public function index(){
			//$this->load->library('session');
			$this->load->helper('form');
			$userdata = $this->session->all_userdata();
			if($userdata['logged_in']){
				echo "Student's Dashboard";
				$this->load->model("studentsdashboard");
				$data['data'] = $this->studentsdashboard->user_array($userdata['id']);
				$this->load->view("studentdashboard",$data);
				$buttonpressed = $this->input->post('button');
				if($buttonpressed){
					redirect('/logout','auto');
				}
			}else{
				redirect('.','auto');
			}
		}
	}
?>
